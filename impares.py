seguir = True
acumulador = 0

while seguir:
    try:
        num1 = int(input("Dame un numero entero no negativo: "))
        num2 = int(input("Dame otro: "))

        if num1 >= 0 <= num2:
            seguir = False
        else:
            print("Introduce un numero positivo: ")
    except:
        print("Introduce un numero entero: ")

for i in range(num1, num2 + 1):
    if i % 2 != 0:
        acumulador += i

print("La suma de", num1, " y ", num2, " es: ", acumulador)